from django.test import TestCase
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIRequestFactory
from shortener.models import ShortenedURL
from shortener.validators import validate_url_hash

def get_url_hash_from_response(response):
    return response.json().get("shortened_url")[-8::]


class ShortenURLAPITestCase(APITestCase):

    def setUp(self):
        self.endpoint = reverse('shortener:shorten_url')

    def test_valid_url_with_scheme_succeeds(self):
        response = self.client.post(
            self.endpoint,
            {"url": "http://www.helloworld.com"})
        self.assertEqual(response.status_code, 201)
        self.assertRegex(get_url_hash_from_response(response), r'[a-zA-Z0-9]{8}')

    def test_valid_url_without_scheme_succeeds(self):
        response = self.client.post(
            self.endpoint,
            {"url": "helloworld.com"})
        self.assertEqual(response.status_code, 201)
        self.assertRegex(get_url_hash_from_response(response), r'[a-zA-Z0-9]{8}')

    def test_invalid_url_fails(self):
        response = self.client.post(
            self.endpoint,
            {"url": "invalid"},
            format='json')

        self.assertEqual(response.status_code, 400)

    def test_unsupported_methods_fail(self):

        for method in ["put", "get", "patch", "head", "delete"]:
            client_method = getattr(self.client, method)
            response = client_method(
                self.endpoint,
                {},
                format='json')
            self.assertEqual(response.status_code, 405)


class AccessLinkTest(TestCase):

    def setUp(self):
        short_url = ShortenedURL.objects.create(url="http://www.helloworld.com")
        short_url.save()
        self.url_hash = short_url.url_hash

    def test_valid_url_hash(self):
        response = self.client.get('/' + self.url_hash)
        self.assertEqual(response.status_code, 301)

    def test_invalid_url_hash(self):
        response = self.client.get('/1234adfr', follow=True)
        self.assertEqual(response.status_code, 404)

    def test_incorrectly_formatted_url_hash(self):
        response = self.client.get('/abc', follow=True)
        self.assertEqual(response.status_code, 404)


class TestURLValidator(TestCase):

    def test_valid_url(self):
        self.assertTrue(validate_url_hash("abcd1234"))

    def test_invalid_url(self):
        self.assertFalse(validate_url_hash("123"))