from django.core.validators import URLValidator
import re


class OptionalSchemeURLValidator(URLValidator):
    def __call__(self, value):
        if '://' not in value:
            # Validate as if it were http://
            value = 'http://' + value
        super(OptionalSchemeURLValidator, self).__call__(value)


def validate_url_hash(url_hash):
    return re.match(r"^[\w+]{8}$", url_hash)
