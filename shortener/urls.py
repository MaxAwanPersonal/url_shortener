from django.conf.urls import url
from shortener import views

urlpatterns = [
    url(r'^shorten_url/', views.ShortenURL.as_view(), name='shorten_url'),
    url(r'^(?P<url_hash>\w+)/$', views.access_link, name='access_link'),
]