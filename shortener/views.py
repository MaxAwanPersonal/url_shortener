from django.shortcuts import render, get_object_or_404, redirect
from rest_framework import generics
from shortener.serializers import URLSerializer
from shortener.models import ShortenedURL
from django.http import HttpResponseRedirect, JsonResponse, HttpResponseNotFound
from shortener.validators import validate_url_hash
from django.core.cache import cache
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.conf import settings

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


class ShortURLMixin(object):

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        shortened_url = request.get_host() + "/" + serializer.data.get("url_hash")
        return JsonResponse({"shortened_url": shortened_url}, status=201)


class ShortenURL(ShortURLMixin, generics.CreateAPIView):
    serializer_class = URLSerializer


def access_link(request, url_hash=None):
    if not validate_url_hash(url_hash):
        return HttpResponseNotFound("Invalid link")
    if url_hash in cache:
        url_map = cache.get(url_hash)
        return HttpResponseRedirect(url_map.url)
    try:
        link = ShortenedURL.objects.get(url_hash=url_hash)
        cache.set(url_hash, link, timeout=CACHE_TTL)
        return HttpResponseRedirect(link.url)
    except (ShortenedURL.DoesNotExist, ShortenedURL.MultipleObjectsReturned):
        # Multiple objects returned should never occur due to atomicity of hash assignment
        return HttpResponseNotFound("Link not found")


def page_not_found(request):
    return render(request, "404.html")


def server_error(request):
    return render(request, "500.html")
