from django.db import models
from .validators import OptionalSchemeURLValidator
# Create your models here.
import urllib
import hashlib
from random import randint
from django.db import transaction


class ShortenedURL(models.Model):
    url = models.CharField(null=False, validators=[OptionalSchemeURLValidator()], max_length=2048)
    url_hash = models.CharField(null=True, blank=True, max_length=8)
    created_at = models.DateField(auto_now_add=True)

    def __repr__(self):
        return self.url_hash

    def save(self, *args, **kwargs):
        super(ShortenedURL, self).save(*args, **kwargs)
        if not self.url_hash:
            self.prepare_url()
            self.assign_url_hash()

    def prepare_url(self):
        if "://" not in self.url:
            self.url = "http://" + self.url

    @transaction.atomic
    def assign_url_hash(self):
        exists = True
        augmented_string = self.url
        while exists:
            augmented_string = str(randint(0, 10)) + augmented_string
            hex_string = hashlib.md5(augmented_string.encode('utf-8')).hexdigest()[:8]
            exists = ShortenedURL.objects.filter(url_hash=hex_string).exists()
        self.url_hash = hex_string
        self.save()