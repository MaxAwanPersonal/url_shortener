from rest_framework import serializers
from shortener.models import ShortenedURL


class URLSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShortenedURL
        fields = ('url', 'url_hash')
        read_only_fields  = ('url_hash',)