# Create new file local.py in this directory, change settings appropriately and do not commit it to git

DEBUG = True

SECRET_KEY = 'ENTER_KEY_HERE'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'db_name',
        'USER': 'db_user',
        'PASSWORD': 'db_password',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}