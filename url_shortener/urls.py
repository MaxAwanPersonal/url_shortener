
from django.conf.urls import include, url
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^', include(('shortener.urls', 'shortener'), namespace='shortener')),
]

handler404 = 'shortener.views.page_not_found'
handler500 = 'shortener.views.server_error'