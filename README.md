# Max's URL Shortener

This app stores your lengthy urls for you and provides you with shortened versions to use.

When these shortened urls are visited, they will redirect you to their respective stored url

## Getting Started

* These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

* linux (ideally a variant of ubuntu installed)
* mysql
* python3
* pip3
* virtualenvwrapper


## Installing

### Set up mysql

* mysql -u username -p                (replace username with your mysql user)
* Enter your password
* CREATE DATABASE url CHARACTER SET UTF8;
* GRANT ALL PRIVILEGES ON url.* to username@localhost; - here replace username with your mysql user
* FLUSH PRIVILEGES;

### Clone the repo and move into the directory

* git clone ...
* cd url_shortener

### Installing requirements

* sudo apt-get install redis-server
* mkvirtualenv --python=/usr/bin/python3 URL
* pip install -r requirements.txt
* cp url_shortener/local_example.py url_shortener/local.py

* Change the settings in url_shortener/local.py to match your mysql credentials

* python manage.py migrate


### Running the app

* workon URL
* sudo redis-server
* python manage.py runserver


## Running the tests

* workon URL
* python manage.py test


There are two test cases

One test case verifies that a correctly entered url will generate a shortened url and that an incorrect one won't
The other verifies that a valid shortened url will redirect to the appropriate url and that an incorrect one won't.


## Endpoints

* **URL**

  `/shorten_url/`

* **Method:**


  `POST`


* **Data Params**

  JSON with body in the following format:

    {
    "url": "www.helloworld.com"
    }
Response:

* **Success Response:**


  * **Code:** 201 <br />
    **Content:** `{ "shortened_url" : [8 digit alphanumeric] }`

* **Error Response:**

  <_Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be._>

  * **Code:** 400 Invalid url entered <br />
    **Content:** `{"url":["Enter a valid URL."]}`


* **Sample Call:**

  Request:
    POST www.your_service.com/shorten_url
        body:
        {
            "url": "www.helloworld.com"
        }

* **URL**

  `/`

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `url_hash=[alphanumeric]`


* **Success Response:**


  * **Code:** 301 <br />
    **Description:** `You should be redirected to the appropriate url`

* **Error Response:**


  * **Code:** 404 UNAUTHORIZED <br />
    **Description:** `Any invalid or incorrect url_hash will return a 404`


* **Sample Call:**

* /abcd1234 should will return the link associated with the url hash abcd1234

## Deployment and Scaling

* For a simple solution, this could be deployed with elastic beanstalk using the following guide
* https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-django.html
- the environment variables are already prepared for use with RDS via Elastic Beanstalk

* This could also be done manually.
* For this, I'd create a docker container for the application and use something like AWS cloud formation to orchestrate.
Within this an application load balancer with an auto scaling group should cater for scaling.
I'd suggest the use of AWS cloudwatch for logging.


* The use of redis for caching url hashes and validation of hashes prior to database querying will dramatically significantly
reduce the number of queries to the database and should lend well to handling thousands of requests per second.

* With regards to creating the url hashes, I've chose to use MD5 as a hashing algorithm which is unfortunately prone to
collisions necessitating checking if the hash already exists prior to creation. To ensure no collisions, the entire
hash assignment function is implemented as an atomic transaction.

* I would have liked to convert a counter into base 62 and use that for certainly unique hashes but that when it comes to
scaling, that would require a separate counting range for each instance and a service such as zookeeper to help orchestrate.
For the purposes of this project, I felt the first 8 characters of the MD5 encoded url would suffice.

* Ideally, I would have created this as a serverless application purely with AWS:
* AWS API gateway to handle both GET and POST requests
* AWS Lambda to hash the url and store it in AWS DynamoDB
* AWS Lambda to retrieve the relevant URL from AWS DynamoDB and redirect the user

* A serverless approach would be a lot cheaper and would also scale easily without the need for orchestration.

## Built With

* [Python 3](https://docs.python.org/) - The language used
* [Django](https://docs.djangoproject.com/en/2.1/) - The web framework used
* [Redis](https://redis.io/) - The caching server used



## Authors

* **Max Awan** *


## Acknowledgments

* Hat tip to anyone whose code was used
